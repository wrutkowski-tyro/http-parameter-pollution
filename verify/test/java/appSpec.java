package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {
    
    @Autowired
    private MockMvc mockMvc;

    static {
        // Virtualbox: http://10.0.2.15:8082/
        // Osx: http://host.docker.internal:8082/
        // Other systems: http://(IP address of docker host):8082/
        System.setProperty("paymenturl", "http://localhost:8082/");
    }

    @Test
    public void should_transferAmount() throws Exception {
        this.mockMvc.perform(post("/").param("action","transfer").param("amount","100"))
            .andExpect(status().isOk())
            .andExpect(content().string("Fake Payment Controller: Successfully transfered $100"));
    }
}
