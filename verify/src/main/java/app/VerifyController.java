package app;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(final HttpServletRequest request) {
        final Action action = new Action(request.getParameter("action"));
        final Amount amount = new Amount(request.getParameter("amount"));

        if (action.isTransfer()) {
            System.out.println("Verify Controller: Going to transfer $" + amount.getValue());
            final RestTemplate restTemplate = new RestTemplate();
            final String fakePaymentUrl = this.paymentUrl; // Internal fake payment micro-service
            final ResponseEntity<String> response = restTemplate
                    .getForEntity(fakePaymentUrl + "?action=transfer&amount=" + amount.getValue(), String.class);
            return response.getBody();
        } else if (action.isWithdraw()) {
            return "Verify Controller: Sorry, you can only make transfer";
        } else {
            return "Verify Controller: You must specify action: transfer or withdraw";
        }
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody String handleException(RuntimeException e) {
        return null;
    }
}