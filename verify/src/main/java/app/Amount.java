package app;

import java.math.BigInteger;

final class Amount {
    
    private final BigInteger value;

    Amount(String value) {
        BigInteger integerValue = new BigInteger(value);
        // validation
        this.value = integerValue;
    }

    BigInteger getValue() {
        return value;
    }

}