package app;

import java.util.List;
import java.util.Arrays;

final class Action {

    private final String value;
    private final List<String> acceptedActions = Arrays.asList("transfer", "withdraw");

    Action(final String value) {
        String lowercaseValue = value.toLowerCase();
        if (!acceptedActions.contains(lowercaseValue)) {
            throw new RuntimeException("Unsupported action");
        }
        this.value = lowercaseValue;
    }

    Boolean isTransfer() {
        return value == "transfer";
    }

    Boolean isWithdraw() {
        return value == "withdraw";
    }
}